<%-- $Id$
include file for delivering audio questions
should be included in file importing DeliveryMessages
--%>
<!--
* $Id$
<%--
***********************************************************************************
*
* Copyright (c) 2004, 2005, 2006 The Sakai Foundation.
*
* Licensed under the Educational Community License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.osedu.org/licenses/ECL-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License. 
*
**********************************************************************************/
--%>
-->



<f:verbatim><br /></f:verbatim>

<%-- this invisible text is a trick to get the value set in the component tree
     without displaying it; 	 will get this to the back end
--%>
<h:outputText escape="false" value="
<input type=\"hidden\" name=\"mediaLocation_#{question.itemData.itemId}\" value=\"jsf/upload_tmp/assessment#{delivery.assessmentId}/question#{question.itemData.itemId}/#{person.eid}/audio_#{delivery.assessmentGrading.assessmentGradingId}.au\"/>" />
<f:verbatim><h1 id="titleQuestion" style="color: #20438f"></f:verbatim>
<h:outputText value="#{question.text} " escape="false"/>
<f:verbatim></h1></f:verbatim>
<!-- ATTACHMENTS -->
<%@ include file="/jsf/delivery/item/attachment.jsp" %>

<f:verbatim><br /></f:verbatim>
<f:verbatim><br /></f:verbatim>
<f:verbatim>
	<div id="</f:verbatim><h:outputText value="question#{question.itemData.itemId}" /><f:verbatim>" style="</f:verbatim><h:outputText value="display:none;" rendered="#{question==null or question.hasNoMedia}" /><f:verbatim>" >
</f:verbatim>
<h:panelGrid cellpadding="10" columns="1">
	<h:panelGroup>	
		<h:outputText escape="false" value="<video controls playsinline><source src=\"/samigo-app/servlet/ShowMedia?mediaId=#{question.mediaArray[0].mediaId}}\" type=\"video/mp4\" />"></h:outputText>
  		<f:verbatim><br /></f:verbatim>
      	<h:outputText value="#{deliveryMessages.open_bracket}"/>
      	<f:verbatim><span id="</f:verbatim><h:outputText value="details#{question.itemData.itemId}" /><f:verbatim>"></f:verbatim>
			<h:outputText styleClass="recordedOn#{question.itemData.itemId}" value="#{deliveryMessages.recorded_on} " rendered="#{!question.mediaArray[0].durationIsOver}" />
			<h:outputText value="#{deliveryMessages.recorded_on} " rendered="#{question.mediaArray[0].durationIsOver}" />
      	<h:outputText value="#{question.mediaArray[0].createdDate}">
        	<f:convertDateTime pattern="#{deliveryMessages.delivery_date_format}" />
      	</h:outputText>
     	 <f:verbatim></span></f:verbatim>
      	<h:outputText value="#{deliveryMessages.close_bracket}"/>
      	<f:verbatim><br /></f:verbatim>
      	<h:outputFormat styleClass="can_you_hear_#{question.itemData.itemId}" value=" #{deliveryMessages.can_you_hear_video}" escape="false">
			<f:param value="<a href=\"#{delivery.protocol}/samigo-app/servlet/ShowMedia?mediaId=#{question.mediaArray[0].mediaId}&setMimeType=false\"/> #{deliveryMessages.can_you_hear_2}</a>" />
      	</h:outputFormat>
    </h:panelGroup>
</h:panelGrid>
<f:verbatim>
	</div>
</f:verbatim>

<f:verbatim><div id="</f:verbatim><h:outputText value="attempts#{question.itemData.itemId}" /><f:verbatim>"></f:verbatim>
<h:panelGroup rendered="#{(question.attemptsRemaining == null || question.attemptsRemaining > 0) && question.hasNoMedia}">
	<f:verbatim>
		<div id="recorder_div">
			<video id="recorder" controls autoplay playsinline></video>
		</div>
	</f:verbatim>  
	<f:verbatim><div id="countdown" style="font-size: large; font-weight: 600; color: #20438f"></div></f:verbatim>
	<f:verbatim><div id="submit-div" class="act"></f:verbatim>
	<f:verbatim><input type="button" class="active" value="Submit video" id="btn-stop-recording" onclick="stopRecording();return false;"></f:verbatim>
	<f:verbatim><p style="font-size: large; font-weight: 600; color: #20438f"></f:verbatim>
	<h:outputText value=" #{deliveryMessages.submit_video_1}"/>
	<f:verbatim></p></div></f:verbatim>
	<f:verbatim><br /></f:verbatim>

</h:panelGroup>
<f:verbatim></div></f:verbatim>

<h:panelGroup rendered="#{question.attemptsRemaining != null && question.attemptsRemaining < 1}">
  <h:outputText value=" #{assessmentSettingsMessages.record_no_more_attempts}"/>
</h:panelGroup>

<h:panelGroup rendered="#{(delivery.actionString=='previewAssessment'
                || delivery.actionString=='takeAssessment' 
                || delivery.actionString=='takeAssessmentViaUrl')
             && delivery.navigation ne '1' && delivery.displayMardForReview }">
<h:selectBooleanCheckbox value="#{question.review}" id="mark_for_review" />
	<h:outputLabel for="mark_for_review" value="#{deliveryMessages.mark}" />
	<h:outputLink title="#{assessmentSettingsMessages.whats_this_link}" value="#" onclick="javascript:window.open('/portal/tool/#{requestScope['sakai.tool.placement.id']}/jsf/author/markForReviewPopUp.faces','MarkForReview','width=300,height=220,scrollbars=yes, resizable=yes');event.preventDefault();" >
		<h:outputText  value=" #{assessmentSettingsMessages.whats_this_link}"/>
	</h:outputLink>
</h:panelGroup>

 <h:panelGroup rendered="#{delivery.feedback eq 'true'}">
  <h:panelGrid rendered="#{delivery.feedbackComponent.showItemLevel && question.feedbackIsNotEmpty}">
    <h:panelGroup>
      <h:outputLabel for="feedSC" styleClass="answerkeyFeedbackCommentLabel" value="#{commonMessages.feedback}#{deliveryMessages.column} " />
      <h:outputText id="feedSC" value="#{question.feedback}" escape="false"/>
    </h:panelGroup>
    <h:outputText value=" "/>
  </h:panelGrid>
  
  <h:panelGrid rendered="#{delivery.actionString !='gradeAssessment' && delivery.feedbackComponent.showGraderComment && !delivery.noFeedback=='true' && (question.gradingCommentIsNotEmpty || question.hasItemGradingAttachment)}" columns="1" border="0">
  	<h:panelGroup>
      <h:outputLabel for="commentSC" styleClass="answerkeyFeedbackCommentLabel" value="#{deliveryMessages.comment}#{deliveryMessages.column} " />
	  <h:outputText id="commentSC" value="#{question.gradingComment}" escape="false" rendered="#{question.gradingCommentIsNotEmpty}"/>
    </h:panelGroup>
    
	<h:panelGroup rendered="#{question.hasItemGradingAttachment}">
      <h:dataTable value="#{question.itemGradingAttachmentList}" var="attach">
        <h:column>
          <%@ include file="/jsf/shared/mimeicon.jsp" %>
        </h:column>
        <h:column>
          <f:verbatim>&nbsp;&nbsp;&nbsp;&nbsp;</f:verbatim>
          <h:outputLink value="#{attach.location}" target="new_window">
            <h:outputText value="#{attach.filename}" />
          </h:outputLink>
        </h:column>
        <h:column>
          <f:verbatim>&nbsp;&nbsp;&nbsp;&nbsp;</f:verbatim>
          <h:outputText escape="false" value="(#{attach.fileSize} #{generalMessages.kb})" rendered="#{!attach.isLink}"/>
        </h:column>
      </h:dataTable>
    </h:panelGroup>
  </h:panelGrid>
</h:panelGroup>
<script type="text/javascript" src="https://www.WebRTC-Experiment.com/RecordRTC.js"></script>

<script>
	  var recorder; 
	  var video = document.getElementById("recorder");

	  function startRecording() {
	      captureCamera(function(camera) {
	          video.muted = true;
	          video.volume = 0;
	          video.srcObject = camera;
	          
	          recorder = RecordRTC(camera, {
	              type: 'video'
	          });
	          recorder.startRecording();
	          recorder.camera = camera;
	          timerToEnd();
	      });
	      
	      return false;
	  };
	  
	  function stopRecording() {
		  $("#submit-div").hide(); 
		  document.getElementById("countdown").innerHTML = "You have submitted the video. Uploading it now..."
	      recorder.stopRecording(stopRecordingCallback);
	      return false;
	  };
	  
	  function stopRecordingCallback() {
	      video.src = video.srcObject = null;
	      video.muted = false;	
	      video.volume = 1;
	      video.src = URL.createObjectURL(recorder.getBlob());
	      
          postVideo(recorder.getBlob());
	      
          recorder.camera.stop();
	      recorder.destroy();
	      recorder = null;
	  }

	  function captureCamera(callback) {
	      navigator.mediaDevices.getUserMedia({ audio: true, video: true }).then(function(camera) {
	          callback(camera);
	      }).catch(function(error) {
	          alert('Unable to capture your camera. Please check console logs.');
	          console.error(error);
	      });
	  }

	  function timerToEnd() {
		  var duration = parseInt(<h:outputText value="'#{question.duration}'" escape="false"/>);
		  var setupTimer = setInterval(function(){
			  if (!recorder){
				  clearInterval(setupTimer); 
			  }
			  if (recorder) { 
				  document.getElementById("countdown").innerHTML = "The recoding will automatically end and will be submitted after " + duration + " seconds";
				}
			  duration -= 1;
			  if(duration <= 0){
			    clearInterval(setupTimer);
			    if (recorder) {
			    	document.getElementById("countdown").innerHTML = "The time to answer has ended. Uploading the video now..."
			    	stopRecording();
			    }
			  }
		}, 1000);
	  }
	  
	  function postVideo(blob) {
		$("#btn-stop-recording").prop('value', 'Uploading video...');
		$("#btn-stop-recording").attr('disabled', true);
		var urlStr = generateURL();
	    var fileObject = new File([blob], getFileName('mp4'), {
	    	type: 'video/mp4'
	  	});
		var formData = new FormData();
		formData.append("file", fileObject);

		$.ajax({
              url: urlStr,
              type: 'POST',
              data: formData,
              contentType: false,
              processData: false,
              cache: false,
              success: function(response) {
					$('input[type=submit]').attr('disabled',false);
                  if (response) {
                	  document.getElementById("countdown").innerHTML = "Video uploaded successfully!";
                  } else {
                	  document.getElementById("countdown").innerHTML = "Failure uploading the video. Please contact sys admin.";
                  }
              }
          });
	  }
	  
	  function generateURL() {
	        var localeLanguage = <h:outputText value="'#{person.localeLanguage}'" escape="false"/>;
	        var localeCountry = <h:outputText value="'#{person.localeCountry}'" escape="false"/>;
	        var unlimitedString = <h:outputText value="'#{deliveryMessages.unlimited}'" escape="false"/>;
	        var agentId = <h:outputText value="'#{person.id}'" escape="false"/>;
	        var maxSeconds = parseInt(<h:outputText value="'#{question.duration}'" escape="false"/>);
	        var attemptsAllowed = <h:outputText value="'#{question.triesAllowed}'" escape="false"/>;
	        var attemptsRemaining = parseInt(<h:outputText value="'#{question.attemptsRemaining}'" escape="false"/>);
	        var paramSeq = <h:outputText value="'#{param.sequence}'" escape="false"/>;
	        var questionId = <h:outputText value="'#{question.itemData.itemId}'" escape="false"/>;
	        var questionNumber = <h:outputText value="'#{question.number}'" escape="false"/>;
	        var questionTotal = <h:outputText value="'#{part.questions}'" escape="false"/>;
	        var assessmentGrading = <h:outputText value="'#{delivery.assessmentGrading.assessmentGradingId}'" escape="false"/>;
	        var deliveryProtocol = <h:outputText value="'#{delivery.protocol}'"/>;
	        var messagesSecs = <h:outputText value="'#{deliveryMessages.secs}'"/>;
	        var recordedOn = <h:outputText value="'#{deliveryMessages.recorded_on}'"/>;
	        var dateFormat = <h:outputText value="'#{deliveryMessages.delivery_date_format}'"/>;
	       	var params = {
	            "localeLanguage": localeLanguage,
	            "localeCountry": localeCountry,
	            "unlimitedString": unlimitedString,
	            "agent": agentId,
	            "maxSeconds": maxSeconds,
	            "attemptsAllowed": attemptsAllowed,
	            "attempts": attemptsAllowed,
	            "paramSeq": paramSeq,
	            "questionId": questionId,
	            "questionNumber": questionNumber,
	            "questionTotal": questionTotal,
	            "assessmentGrading": assessmentGrading,
	            "deliveryProtocol": deliveryProtocol,
	            "messagesSecs": messagesSecs,
	            "recordedOn": recordedOn,
	            "dateFormat": dateFormat
	        };
	    
	     var url = <h:outputText value="'#{delivery.protocol}/samigo-app/servlet/UploadVideo?media=jsf/upload_tmp/assessment#{delivery.assessmentId}/question#{question.itemData.itemId}/#{person.eid}/video_#{delivery.assessmentGrading.assessmentGradingId}_#{question.itemData.itemId}'" />;	
	       	
		  for (var key in params) {
		      if (url != "") {
		    	  url += "&";
		      }
		      url += key + "=" + encodeURIComponent(params[key]);
		  }
		  
		  return url;
	  }

	  function getFileName(fileExtension) {
          var d = new Date();
          var year = d.getUTCFullYear();
          var month = d.getUTCMonth();
          var date = d.getUTCDate();
          var rstr = Math.random().toString(36).substring(7);
          return 'RecordRTC-' + year + month + date + '-' + rstr + '.' + fileExtension;
      }
      
	  function main() {
		  $("#submit-div").hide(); 
		  $("#recorder_div").hide(); 
		  var question =  $("#titleQuestion").text().replace(/[|&;$%@"<>()+,]/g, "");
		  var timeToAnswer = 60;
		  var hasNoMedia = (<h:outputText value="'#{question.hasNoMedia}'" escape="false"/> == 'true');
		  if (hasNoMedia) {
			  var setupTimer = setInterval(function(){
				  document.getElementById("countdown").innerHTML = "The recording will start after " + timeToAnswer + " seconds";
				  timeToAnswer -= 1;
				  if(timeToAnswer <= 0){
				    clearInterval(setupTimer);
					document.getElementById("countdown").innerHTML = "";
					$("#recorder_div").show(); 
				    $("#submit-div").show(); 
					startRecording();
				  }
			}, 1000);
		}
	  }
	  main();

	  $(function() {
		var hasNoMedia = (<h:outputText value="'#{question.hasNoMedia}'" escape="false"/> == 'true');
		if (hasNoMedia) {
			$('input[type=submit]').attr('disabled',true);
		}
	  });
</script>

